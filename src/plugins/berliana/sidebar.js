// Berliana sidenav

window.$ = require('jquery')
let ww = $(window).width()

function mq () {
  return {
    'sm': ww < 575.98,
    'xs': ww > 575.98 && ww < 767.98,
    'md': ww > 767.98 && ww < 991.98,
    'lg': ww > 991.98 && ww < 1199.98,
    'xl': ww > 1199.98,
  }
}

let sidenavState = true
let theSize = '250px'
function state () {
  if (sidenavState) {
    theSize = '4.6rem'
  }
  else {
    theSize = '250px'
  }
}

$(window).on('load', function () {
  let wrapperSidenav = $('.wrapper-sidenav')
  let navbar = $('.sidenav-navbar')
  let sidenav = $('.sidenav')
  let toggleSidenav = $('#toggle-sidenav')

  // initial default state
  wrapperSidenav.css('margin-left', theSize)
  sidenav.css('width', theSize)
  sidenav.find('p').css('opacity', 1)

  $(window).resize(function () {
    ww = $(window).width()
    if (mq().sm || mq().xs) {
      wrapperSidenav.css('margin-left', '0')
      sidenav.css('width', '0')
      navbar.hide()
      sidenav.hide()
    } else {
      wrapperSidenav.css('margin-left', theSize)
      sidenav.css('width', theSize)
      navbar.show()
      sidenav.show()
    }
  })

  toggleSidenav.click(function () {
    // if sidenav is open then close it
    if (sidenavState) {
      state()
      wrapperSidenav.css('margin-left', theSize)
      sidenav.css('width', theSize)
      sidenav.find('p').css('opacity',0)
      sidenavState = false
    }
    // if sidenav is closed then open it
    else {
      state()
      wrapperSidenav.css('margin-left', theSize)
      sidenav.css('width', theSize)
      sidenav.find('p').css('opacity', 1)
      sidenavState = true
    }
  })
})
