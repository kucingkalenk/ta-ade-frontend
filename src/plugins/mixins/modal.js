import Vue from 'vue'

Vue.mixin({
  data() {
    return {

    }
  },
  methods: {
    $deleteModal () {
      return new Promise ( (resolve, reject) => {
        this.$bvModal.msgBoxConfirm('Please confirm that you want to delete this data.', {
          title: 'Please Confirm',
          size: 'sm',
          buttonSize: 'sm',
          okVariant: 'danger',
          okTitle: 'YES',
          cancelTitle: 'NO',
          footerClass: 'p-2',
          hideHeaderClose: false,
          centered: true
        })
        .then(value => {
          // return value
          resolve(value);
        })
        .catch(err => {
          reject(err);
        })
      })
    }
  },
})