import Vue from 'vue'

Vue.mixin({
  data() {
    return {

    }
  },
  methods: {
    make(variant = 'primary', message = null, title = null) {
      this.$bvToast.toast(message, {
        title: title,
        variant: variant,
        solid: true
      })
    },
    $toastAdded() {
      this.make('success', 'The data has been added successfully!', 'Success!')
    },
    $toastUpdated() {
      this.make('success', 'The data has been updated successfully!', 'Success!')
    },
    $toastDeleted() {
      this.make('success', 'The data has been deleted!', 'Success!')
    }
  }
})