import './berliana/sidebar'

String.prototype.trunc = function (n, useWordBoundary) {
  if (this.length <= n) {
    return this
  }
  var subString = this.substr(0, n - 1)
  return (useWordBoundary ? subString.substr(0, subString.lastIndexOf('')) : subString) + "..."
}

String.prototype.htmlReplace = function (n) {
  return this.replace(/<(?:.|\n)*?>/gm, n)
}