import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import app from '../main'

Vue.use(VueAxios, axios)

axios.defaults.baseURL = 'http://localhost:8000/api'

axios.interceptors.request.use(config => {
  app.$Progress.start()
  return config
})

axios.interceptors.response.use(response => {
  app.$Progress.finish()
  return response
})