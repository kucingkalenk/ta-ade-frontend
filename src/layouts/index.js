import Vue from 'vue'
import Dashboard from '@/layouts/Dashboard.vue'
import Landing from '@/layouts/Landing.vue'

Vue.component('dashboard-layout', Dashboard)
Vue.component('landing-layout', Landing)