import '@babel/polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// plugins
import '@/plugins/bootstrap-vue'
import '@/plugins/vue-mq'
import '@/plugins/fontawesome'
import '@/plugins/axios'
import '@/plugins/moment'
import '@/plugins/vue-progressbar'
import '@/plugins/vue-datetime'
import '@/plugins/wysiwyg'
import '@/plugins/berliana'
import '@/plugins/mixins/index'
// end plugins

import '@/styles/global.scss'
import '@/styles/bootstrap-custom.scss'

import '@/layouts/index'


Vue.config.productionTip = false

export default new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
