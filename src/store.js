import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    coba: 'Hello',
    tes: 'World!',
    sidebar: {
      marginL: ''
    }
  },
  mutations: {
    fungsi (state, n) {
      console.log(`${state.coba} ${state.tes} ${n}`)
    }
  },
  actions: {

  }
})
