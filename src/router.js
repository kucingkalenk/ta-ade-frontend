import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/dashboard',
      name: 'dashboard',
      meta: {
        layout: 'dashboard'
      },
      component: () => import('@/views/Home.vue')
    },
    {
      path: '/',
      name: 'welcome',
      meta: {
        layout: 'landing'
      },
      component: () => import('@/views/Welcome.vue')      
    },
    {
      path: '/peserta',
      name: 'peserta',
      meta: {
        layout: 'dashboard'
      },
      component: () => import('@/views/Peserta.vue')
    },
    {
      path: '/ujian',
      name: 'ujian',
      meta: {
        layout: 'dashboard'
      },
      component: () => import('@/views/Ujian.vue')
    },
    {
      path: '/ujian/:id',
      meta: {
        layout: 'dashboard'
      },
      component: () => import('@/views/Soal.vue')
    }
  ]
})
