const express = require('express');
const serveStatic = require("serve-static")
const history = require('connect-history-api-fallback')
let argv = require('minimist')
const path = require('path')

const app = express()
app.use(history())

const staticFileMiddleware = express.static(path.join(__dirname, 'dist'))
app.use(staticFileMiddleware)
app.use(history({
  disableDotRule: true,
  verbose: true
}))
app.use(staticFileMiddleware)
// app.use(serveStatic(path.join(__dirname, 'dist')));
const port = process.env.PORT || 8080
app.listen(port,argv.ip)
console.log('running on port 8080')
